FROM debian:9 as build
RUN apt update && apt install -y wget gcc make
RUN apt install -y libpcre3 libpcre3-dev
RUN apt install -y zlib1g-dev
RUN wget https://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure &&  make &&  make install




FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx/ .
RUN mkdir -p usr/local/nginx/logs /usr/local/nginx/conf && touch usr/local/nginx/logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off; "]


